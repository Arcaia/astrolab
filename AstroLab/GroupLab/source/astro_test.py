#!/usr/bin/env python3

import datetime
import io
from unittest.mock import Mock

import astro


def test_get_location1():
    """Test get_location1 function to confirm it returns the corect city
    and state(region) for a given longitude and lattitude
    """
    city, state = astro.get_location1("41.854264", "-87.619241")
    assert city == "Chicago"
    assert state == "IL"


def test_get_location():
    """Test get_location function is returning the correct location info
    for the default ip address
    """
    lat1, lon1, city, region = astro.get_location()
    assert lat1 == 41.8536
    assert lon1 == -87.6183
    assert city == "Chicago"
    assert region == "IL"


def test_print_position():
    """Test formatting of print_position method"""
    testdate = datetime.datetime(2021, 4, 22, 14, 58)
    output = io.StringIO()
    azimuth = "233°28'12\""
    altitude = "49°16'12\""
    city = "Chicago"
    region = "IL"
    curdate = testdate.date()
    current_time_display = testdate.strftime("%I:%M %p")
    dis = "150393241"
    mag = "-26.73"
    output = io.StringIO()
    body = "sun"

    expected_result = """From Chicago, IL at 02:58 PM on April 22, 2021: 
            sun: 
                Distance from Earth: 150,393,241 km
                Magnitude: -26.73 
                Position: 
                    Azimuth: 233 deg 28 min 12 sec 
                    Altitude: 49 deg 16 min 12 sec
    """

    astro.print_position(
        azimuth,
        altitude,
        city,
        region,
        curdate,
        current_time_display,
        dis,
        mag,
        body,
        output,
    )
    #    print(output.getvalue())
    #    print(expected_result)
    assert expected_result.strip() == output.getvalue().strip()


# def test_get_sun_position():
#    """[summary]"""
#    azi, alt, curdate, current_time, mag, dis, current_time_display = get_sun_position(
#        41.8536, -87.6183
#    )

if __name__ == "__main__":
    test_print_position()
