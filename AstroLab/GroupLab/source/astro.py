#!/usr/bin/env python3
import requests
import datetime
import argparse
import sys

# import io
# from pprint import pprint
from requests.auth import HTTPBasicAuth


ASTRONOMYAPI_ID = "dc1ed6a3-50e4-4f68-a806-244fa270e1aa"
ASTRONO_MYAPI_SECRET = "9c0bcbf61df377fe3813424fb593d145142f998f892dfac72a7703c9d46c726673dcdbb2c8e1a2e707321e194c58a0896e3130d9000f5dd80a4144d1617d0e9be985eace8122ffdd61b5b82c2088aaf7faaeac4f188ad53686af715b4469850cb0ea7e4605054c38e9b6a77181c1f6c3"

POSITION_STACK_KEY = "ca30f2a7d8132e388e4b7b1177dec61d"


def get_location1(longitude, latitude):
    """Returns the City and Region based on the longitude and latitude provided"""

    url = "http://api.positionstack.com/v1/reverse"

    coords = longitude + "," + latitude

    payload = {"access_key": POSITION_STACK_KEY, "query": coords, "output": "json"}
    try:
        response = requests.get(url, params=payload)
        # print(response.status_code)
    except requests.ConnectionError:
        if response.status_code != 200:
            print("\n", "Website Error: ", url, response, "\n")
            sys.exit()

    body_info = response.json()

    city = body_info["data"][0]["locality"]
    state = body_info["data"][0]["region_code"]

    return city, state


def get_location():
    """Returns the longitude and latitude for the location of this machine.Returns:str: latitudestr: longitude"""

    ip = "165.225.60.205"
    url = "http://ip-api.com/json/" + ip

    response = requests.get(url)
    ip_info = response.json()

    lat = ip_info["lat"]
    lon = ip_info["lon"]
    city = ip_info["city"]
    region = ip_info["region"]

    return lat, lon, city, region


def get_sun_position(
    latitude, longitude, body="sun", datetimenow=datetime.datetime.now()
):
    """Returns the current position of the sun in the sky at the specified location

    Parameters:latitude (str)longitude (str)
    Returns:float: azimuthfloat: altitude"""

    curdate = datetime.date.today()
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    current_time_display = now.strftime("%I:%M %p")

    # create datetime object - remove curdate, now - refactor above.

    payload = {
        "latitude": latitude,
        "longitude": longitude,
        "from_date": curdate,
        "to_date": curdate,
        "time": current_time,
        "elevation": 0,
    }

    url = "https://api.astronomyapi.com/api/v2/bodies/positions/" + body
    response = requests.get(
        url, params=payload, auth=HTTPBasicAuth(ASTRONOMYAPI_ID, ASTRONO_MYAPI_SECRET)
    )
    body_info = response.json()

    cells = body_info["data"]["table"]["rows"][0]["cells"][0]
    azi = cells["position"]["horizonal"]["azimuth"]["string"]
    alt = cells["position"]["horizonal"]["altitude"]["string"]
    mag = cells["extraInfo"]["magnitude"]
    dis = cells["distance"]["fromEarth"]["km"]

    return azi, alt, curdate, current_time, mag, dis, current_time_display


def print_position(
    azimuth,
    altitude,
    city,
    region,
    curdate,
    current_time_display,
    dis,
    mag,
    body="Sun",
    output=sys.stdout,
):
    """Prints the position of the sun in the sky using the supplied coordinates
    Parameters:azimuth (float)altitude (float)
    """

    for r in (("\N{DEGREE SIGN}", " deg "), ("'", " min "), ('"', " sec")):
        azimuth = azimuth.replace(*r)
        altitude = altitude.replace(*r)

    if output != sys.stdout:

        print(
            f"""From {city}, {region} at {current_time_display} on {curdate:%B %d, %Y}: 
            {body}: 
                Distance from Earth: {"{0:,.0f}".format(float(dis))} km
                Magnitude: {"{0:,.2f}".format(float(mag))} 
                Position: 
                    Azimuth: {azimuth} 
                    Altitude: {altitude}""",
            file=output,
        )
    else:
        print(
            f"""From {city}, {region} at {current_time_display} on {curdate:%B %d, %Y}: 
            {body}: 
                Distance from Earth: {"{0:,.0f}".format(float(dis))} km
                Magnitude: {"{0:,.2f}".format(float(mag))} 
                Position: 
                    Azimuth: {azimuth} 
                    Altitude: {altitude}"""
        )


#    print(output.getvalue())


def getparms():
    """Evaluates arguments submitted with command to determine if longitude,
    latitude, and body name have been provided or if the default functionality
    is being requested

    Returns:
        lat: latitude argument
        long: longitude argument
        body: argument submitted for name of planet to use in query
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--lat", help="latitude", type=str)
    parser.add_argument("--long", help="longitude", type=str)
    parser.add_argument("--body", help="body", type=str)
    parser.parse_args()
    args = parser.parse_args()

    return args.lat, args.long, args.body


if __name__ == "__main__":
    # getparms()
    # lat1="41.854264"
    # lon1="-87.619241
    lat1, lon1, body = getparms()
    if lat1 is None:
        lat1, lon1, city, region = get_location()
    else:
        city, region = get_location1(lat1, lon1)

    if body is None:
        body = "sun"

    azi, alt, curdate, current_time, mag, dis, current_time_display = get_sun_position(
        lat1, lon1, body
    )
    print_position(
        azi,
        alt,
        city,
        region,
        curdate,
        current_time_display,
        dis,
        mag,
        body.capitalize(),
    )
